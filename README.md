# Il Trashware
Libro introduttivo sul Trashware e l'uso base del computer,
versione curata da GOLEM, basata su un'opera originaria di officina-s3.org

La versione compilata è disponibile fra le [dailybuilds](https://golem.linux.it/pubblici/dailybuilds/s3book/main.pdf).

## Sorgenti
	main.tex
	tex/
	img/

## Software necessari
* pdflatex e pacchetti LaTeX (nel dubbio: texlive-*)
* inkscape, per la conversione automatica delle immagini vettoriali

## Compilazione
	make [book]

## Distribuzione
File PDF sotto dist/

# Regole operative per la scrittura
* Affinché, fra una versione e l'altra del documento, si preservino le differenze in modo più o meno congruo, si sconsiglia di spezzare parti di periodo con ritorni a capo (ad esempio per giustificare il codice);
* Utilizzare `` per aprire le virgolette e " per chiuderle;
* Utilizzare \emph per enfatizzare parole nel testo, e non abusare di \textit
