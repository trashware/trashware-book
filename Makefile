### SVG conversion parameters ###
SVG_DIR = ./img/originals
VEC_DIR = ./img/vectors

# Select all SVG filenames
SVG   = $(notdir $(wildcard $(SVG_DIR)/*.svg))
# Gets PDF filenames
PDF   = $(SVG:.svg=.pdf)

# Where are SVG?
vpath %.svg $(SVG_DIR)
vpath %.pdf $(VEC_DIR)

# Select all raster
RASTER   = $(notdir $(wildcard .img/raster/*))

### TEX files ###
TEX_DIR = ./tex
TEX     = $(notdir $(wildcard $(TEX_DIR)/*.tex))
# Where are TEX?
vpath %.tex $(TEX_DIR)

all: dist/main.pdf figure

dist/main.pdf: main.tex $(TEX) $(PDF) $(RASTER)
	mkdir -p build/tex dist
	git rev-parse HEAD > build/commit.tex
	pdflatex -output-directory build -synctex=1 -interaction=nonstopmode main.tex
	pdflatex -output-directory build -synctex=1 -interaction=nonstopmode main.tex
	mv build/main.pdf dist
	mv build/main.synctex.gz dist

figure: $(PDF)

%.pdf : %.svg
	@mkdir -p $(VEC_DIR)
	@echo "Sto generando $@"
	# Conversion command
	inkscape -z -D --file=$^ --export-pdf=$(VEC_DIR)/$@

clean:
	rm -rf build
	rm -rf $(VEC_DIR)

mrproper: build
	rm -rf dist
