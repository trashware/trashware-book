\chapter{Software}

\begin{shadequote}
Dagli vita! Dagli vita, capito?! Dai vita alla mia creatura!
\par\emph{Gene Wilder} mentre interpreta il \emph{Dr. Frankenstin}
\end{shadequote}

Nella lingua inglese software nasce per contrapposizione al termine ``hardware" (letteralmente, ``parte dura") e dalla composizione delle parole ``soft" e ``ware'', che in italiano significano, rispettivamente, ``morbido", ``tenero", ``leggero" e ``merci", ``articoli", ``prodotti".

In pratica il termine definisce programmi e procedure utilizzati per far eseguire al computer un determinato compito.
Tutto quello che in un computer non riusciamo a toccare con le mani è considerato software.
Il software viene, a grandi linee, suddiviso in:

\paragraph{Software di base}
È la parte del software più vicina alla macchina, indispensabile per il funzionamento del computer.
Viene generalmente identificato con il sistema operativo.

\paragraph{Software applicativo}
Viene definito software applicativo (o semplicemente ``applicativi" o ``applicazioni") quell'insieme di programmi che l'utente usa per svolgere operazioni quotidiane: per scrivere una email o un documento, ritoccare una foto o navigare sul web.
Se ne possono individuare alcune categorie:

\begin{itemize}
	\item ambienti desktop, o interfacce grafiche: insiemi di applicazioni per consentire agli esseri umani di interagire col computer tramite figure e immagini;
	\item accessori: utilità comuni, come calcolatrici, blocchetti per appunti;
	\item giochi: applicazioni per lo svago, come giochi di carte e scacchiere;
	\item didattica: per l'ambito scolastico e accademico, come planetari e tavole periodiche;
	\item grafica: per la gestione di immagini e ritocco delle fotografie;
	\item internet: per la navigazione sul web, la posta elettronica e altri servizi della rete;
	\item multimediali: per la riproduzione e la realizzazione di musica e filmati;
	\item sistema: per l'amministrazione del software di base, come gestione dei file nel filesystem;
	\item sviluppo: per la realizzazione di nuove applicazioni;
	\item ufficio: per la produzione di documenti, come lettere e amministrazione contabile, molto utili anche in ambito domestico;
\end{itemize}

\section{Sviluppo del software}
Il computer può essere utilizzato solamente se questo è composto sia da hardware che da software: infatti, un computer senza software, non è altro che un circuito elettronico e qualche pezzo di ferro, mentre senza hardware, non è altro che un'idea irrealizzabile.
In un certo senso, l'hardware costituisce il braccio, ed esegue il lavoro che gli viene impartito dalla mente, costituita dal software.

La realizzazione di hardware e software è molto complessa, ma, qualunque essere umano utilizzi il computer per la sua ``intelligenza", deve almeno conoscere a grandi linee come viene realizzato il software, per poter indirizzare le proprie scelte in maniera consapevole.

Il software è costituito da una sequenza di comandi e istruzioni che l'hardware deve eseguire; l'hardware conosce solo poche istruzioni, molto elementari, che devono essere codificate con dei numeri.
Se i programmi dovessero essere scritti sottoforma di numeri, realizzare programmi complessi come quelli odierni sarebbe estremamente difficile, per non dire impossibile.
Pertanto, i programmatori hanno inventato dei \emph{linguaggi di programmazione}, con i quali vengono scritti i programmi in una forma detta \emph{sorgente}. Una volta scritti i codici sorgenti, essi vengono tradotti in numeri, la cosiddetta forma \emph{binaria}.
Affinché il programma possa essere effettivamente eseguito, il codice sorgente deve quindi essere tradotto in codice binario.

\begin{figure}
	\begin{minipage}{.7\linewidth}
		\begin{Verbatim}
int main(int argc, char** argv) {
 int risultato = 1, base = 2, esponente = 4;
 for (int i = 0; i < esponente; ++i) {
  risultato = risultato * base;
 }
}
		\end{Verbatim}
	\end{minipage}
	\begin{minipage}{.25\linewidth}
		\begin{Verbatim}
55
48 89 e5
c7 45 f0 01 00 00 00
[...]
8b 45 f4
3b 45 fc
7d 10
8b 45 f0
0f af 45 f8
89 45 f0
83 45 f4 01
eb e8
b8 00 00 00 00
5d
c3
		\end{Verbatim}
	\end{minipage}
	\caption{Confronto tra un codice sorgente in linguaggio C e il suo corrispondente codice binario}
	\label{fig:confronto-sorgente-binario}
\end{figure}

Una volta ottenuto un binario, è molto difficile (e praticamente impossibile) risalire al sorgente che l'ha generato, mentre ottenere il binario dal sorgente è una questione relativamente veloce. Inoltre, il sorgente è sì scritto in un linguaggio tecnico, ma è comprensibile da un essere umano in poco tempo, mentre il binario no, come ci si può accorgere, anche da profani, dal raffronto in \figurename~\ref{fig:confronto-sorgente-binario}.

Volendo fare un paragone culinario, il codice sorgente è la ``ricetta" che permette di cucinare una torta. Per mangiare, la ricetta deve essere tradotta nella torta, e per poter ottenere la torta, si deve avere la ricetta. Non è possibile risalire alla ricetta dalla torta.

Detto questo, cosa deve fare l'utilizzatore accorto di un computer quando vuole utilizzare un programma?

Sotto il punto di vista tecnico, per poter utilizzare un programma, è sufficiente il suo codice binario.
Tuttavia, in pratica devono essere analizzati anche alcuni aspetti legali che legano indissolubilmente il codice sorgente col codice binario, così come ogni ricetta è legata alla sua torta.

Nasce quindi l'esigenza di comprendere alcune licenze d'uso.

\section{Licenze e libertà}
\subsection{Software proprietario}
In generale il software proprietario è un applicativo di cui la riproduzione, modifica, distribuzione e utilizzo sono vincolate a una richiesta di permesso o categoricamente proibite.
Rispetto ad altri prodotti dell'ingegno, il codice sorgente può essere occultato.
Spesso, quindi, il codice sorgente del software proprietario non è reso pubblico proprio per ostacolare chi volesse violare le clausole che lo proteggono e leggi ben precise ne impediscono lo studio e la ridistribuzione (copyright).

La licenza che accompagna il software proprietario è genericamente chiamata \textbf{EULA}, ossia \emph{End User License Agreement}, traducibile in italiano con \emph{accordo di licenza con l'utilizzatore finale}.
Quando il software proprietario è a pagamento, ogni produttore fornisce una propria EULA personalizzata. Quando invece è gratuito, le EULA possono essere ulteriormente suddivise nelle seguenti categorie:

\begin{itemize}
	\item \textbf{freeware} se è sottoposto esplicitamente ad una licenza che ne permette la redistribuzione gratuita. Il software freeware pur essendo proprietario, viene concesso in uso senza alcun corrispettivo, ed è liberamente duplicabile e distribuibile, con pochissime eccezioni.
	\item \textbf{shareware} se è disponibile gratuitamente solo per un periodo di prova, nonostante sia distribuito in maniera simile a quello freeware, richiede un pagamento al termine di un periodo di prova o per attivarne tutte le funzionalità che sono state limitate.
	\item \textbf{abandonware} se si tratta di software non più mantenuto né commercializzato da tempo, pertanto è considerato ``abbandonato" ed esplicitamente rilasciato come di pubblico dominio.
\end{itemize}

\subsection{Software libero}
Il Software Libero e Open Source\footnote{le due terminologie \emph{software~libero} e \emph{open~source}, nella pratica, indicano lo stesso tipo di software, tuttavia esistono delle ragioni filosofiche alla base di questa distinzione, che non sono oggetto di questo manuale} identifica programmi non sottoposti ad alcun vincolo, lasciando così l'utente libero di farne ciò che desidera, per esempio di:
\begin{itemize}
	\item eseguirlo per qualsiasi scopo;
	\item studiarlo e modificarlo;
	\item migliorarne le funzionalità;
	\item poterlo liberamente ridistribuire;
\end{itemize}

Il software libero viene sempre distribuito in forma sorgente, e quasi sempre anche in forma binaria. Nel rarissimo caso in cui la forma binaria non fosse disponibile, rimane comunque sempre possibile ottenerla in autonomia effettuando la traduzione per proprio conto.

Come il software proprietario, anche quello libero può essere a pagamento, oppure gratuito, anche se generalmente si trova in quest'ultima forma.

È necessario porre l'attenzione sul fatto che molti confondono ``free software" con software ``gratis", poiché “free” in inglese vuol dire sia gratuito che libero, ma quando parliamo di ``free software" intendiamo il software libero.

Tra le più famose licenze per il software libero si ricordano la APL (Apache Public Licence), la \textbf{BSD} (Berkley Software Distribution), la EUPL (European Union Public Licence), la \textbf{GPL} (General Public Licence) e similari (quali AGPL e LGPL) e la \textbf{MPL} (Mozilla Public Licence).

Curiosità: in alcuni casi, può accadere che uno stesso programma venga rilasciato sia con una licenza proprietaria sia con una licenza libera: in questo caso, la prima è spesso dedicata ad un ambiente professionale, mentre la seconda all'utenza domestica.

\subsection{Vantaggi del Software Libero}
Utilizzare Software Libero, è prima di tutto una scelta di natura etica.
Il suo sviluppo si basa sugli stessi principi fondanti della comunità scientifica, senza i quali la ricerca non può progredire: il libero scambio delle informazioni, la condivisione di idee e risultati ed il libero utilizzo del patrimonio comune delle conoscenze per un ulteriore sviluppo.
Favorisce dunque l'indipendenza tecnologica, la diffusione del sapere, l'abbassamento delle barriere di accesso alla tecnologia, stimola la concorrenza e dà sostegno all'economia locale.

Le amministrazioni pubbliche sono incoraggiate dalla legge ad usare software libero poiché impiegano le risorse dei cittadini e dovrebbero preferire l'utilizzo e lo sviluppo di un software che resti a disposizione di tutti.

Dal punto di vista tecnico permette la verificabilità del software: diventa possibile, quando serve, monitorare il comportamento effettivo dei programmi intervenendo direttamente sui problemi, perché il codice sorgente è disponibile; inoltre, consente un'estrema facilità di sviluppo, dal momento che ogni nuova implementazione può basarsi sulle modifiche precedenti.
Il codice sorgente è sottoposto e revisionato da un incalcolabile numero di programmatori, che correggono bug e malfunzionamenti in tempi rapidissimi.
Poiché il codice è sotto gli occhi del mondo, diventa difficile programmare applicativi malevoli.
Infine, chiunque abbia basi di programmazione può contribuire direttamente scrivendo del codice, ma anche traducendo programmi o segnalando problemi da correggere (bug).

% questo discorso lo riprenderei da madbob [se un programmatore alle prime armi mette le mani su software pubblico ha modo di mettersi in mostra]
Dal punto di vista sociale, utilizzare software libero riveste un grande valore culturale dovuto al carattere pubblico e alla condivisione dei risultati; favorisce lo sviluppo professionale: basandosi su una economia dei servizi, incentiva la crescita professionale e l'aumento delle competenze sul territorio.
% va detto meglio... non è così scontato
Dal punto di vista economico, il Software Libero stimola la concorrenza e garantisce grandi possibilità di sviluppo che favoriscono l’economia locale.
%ok formazione
Crea nuove opportunità di business nel campo della formazione e del supporto.
Collaborando con sviluppatori volontari e utilizzando il lavoro della comunità, anche le piccole e medie imprese sono in grado di sviluppare e vendere prodotti di alta qualità.

\subsection{Svantaggi del Software Libero}
Lo sviluppo del software libero può avere una gestione ``anarchica", che porta a risultati incoerenti e ad una mancanza di uniformità.
Può succedere che alcune tipologie di software, soprattutto di nicchia, non siano disponibili, mancando comunità che ne supportino lo sviluppo, oppure al contrario che gruppi di lavoro distinti lavorino a progetti del tutto equivalenti, dando luogo ad una dispersione negli obiettivi.

Anche se spesso è disponibile molta documentazione, è sovente incompleta, non in lingua italiana e a volte non aggiornata.

Parte dello sviluppo del software libero è rappresentato dal lavoro volontario, pertanto può essere più lento rispetto al software proprietario.

\section{GNU/Linux}
% la definizione sua non mi garba... agli occhi della gente mi sembra circolare
Nella prima metà degli anni 80 del XX secolo, Richard~Matthew~Stallman, al tempo ricercatore universitario del MIT\footnote{Massachuttes Institute of Technology di Boston}, decise che avrebbe sviluppato un intero sistema operativo libero, che battezzò GNU\footnote{il sistema si ispirava ad un sistema già esistente del 1969, chiamato Unix: il nome GNU fu scelto come acronimo ricorsivo che sta per \emph{GNU is Not Unix}}.
Tuttavia, il progetto era molto ambizioso, e si dovette aspettare il 1991 prima che, in maniera del tutto indipendente, Linus~Torvalds, un brillante studente universitario finlandese, scrivesse la base di un sistema operativo che andava perfettamente a integrarsi con il software GNU, dando così origine a un sistema operativo completo: GNU/Linux.
Nel frattempo, nel 1989, Stallman aveva redatto la prima versione della licenza libera GPL, con la quale verrà rilasciato anche Linux dal 1992.

\subsection{Perché usare Linux}
\begin{itemize}
	\item \textbf{Sicurezza:} Linux si ispira ai sistemi di tipo Unix, dotati di un'organizzazione a più livelli di privilegio, che consente di tenere fortemente separate le operazioni ordinarie da quelle di modifica e  manutenzione del sistema. Questo garantisce maggiore sicurezza in quanto limita i danni dovuti a errori accidentali, e neutralizza alla radice il software maligno, rendendo Linux praticamente immune alla maggior parte dei virus informatici;
	\item \textbf{Stabilità:} l'architettura Unix è particolarmente indicata anche per l'esecuzione di molte applicazioni, ognuna delle quali agisce nel suo ambito di competenza senza inteferire con le altre, e senza che, in caso di malfunzionamenti, possa bloccare l'intero sistema; questa differenza, ad oggi, è molto meno accentuata rispetto ad alcuni anni fa, in quanto anche i sistemi proprietari si sono evoluti;
	% che è software libero lo possiamo omettere da qui, lo dobbiamo aver già detto in tutte le salse
	\item \textbf{Legalità:} l'utente domestico che utilizza costoso software proprietario può facilmente cadere nella tentazione di utilizzare versioni ``pirata" illegali delle applicazioni di cui ha bisogno; la quasi totalità del software libero è invece gratuita, e viene fornita fin da subito con il sistema, perciò il rischio e la tentazione di utilizzare software illegale diminuisce drasticamente;
	\item \textbf{Riuso:} la maggior parte degli utenti naviga in rete, legge e scrive email, scrive file di testo e presentazioni, dunque non ha bisogno di aggiornare il proprio computer ogni 3/4 anni per avere sempre l'ultimo modello.
	Purtroppo i sistemi proprietari evolvono sempre adattandosi al nuovo hardware, aggiungendo effetti grafici tanto carini quanto inutili, o appesantendo il sistema di base, che, col passare del tempo, gira sempre più lentamente.
	Al contrario, Linux ha un'architettura molto modulare che gli permette di adattarsi sia a computer di ultima generazione che a macchine più datate; in più, alcune versioni sono pensate per l'uso su computer più datati, che possono quindi eseguire in tranquillità tutte le operazioni di base.
\end{itemize}

Altre motivazioni possono essere trovate sul sito \url{http://whylinuxisbetter.net/it/}

\subsection{Le versioni di Linux}
Le diverse versioni di Linux sono dette \textbf{distribuzioni}, o anche semplicemente \emph{distro}, e insieme al sistema operativo raccolgono software applicativi per l'uso base o per guidare l'utente nel processo di installazione del sistema.
Ogni distribuzione si differenzia dall'altra per scelte progettuali, per lo stile grafico, per il bacino di utenti che intende soddisfare e per la ``leggerezza" (l'essere più o meno adatta a computer vecchi).

Tra le distribuzioni più famose si possono citare:

\begin{minipage}{.2\linewidth}
  \includegraphics[width=.9\linewidth]{img/pdf/distro/logo-linuxmint.pdf}
\end{minipage}
\begin{minipage}{.75\linewidth}
\textbf{Linux Mint} è una distribuzione facile da utilizzare, dotata di una grande varietà di software libero di qualità, distribuito attraverso un software~center grafico che permette di installare i programmi con un click. Dispone anche di numerosi strumenti automatici e manuali per guidare l'utente verso l'installazione di software aggiuntivi, come driver e codec multimediali, per sfruttare appieno le caratteristiche tecniche della macchina.
Una nuova versione di Linux~Mint viene rilasciata ogni due anni, e gli aggiornamenti durano per cinque anni.
Linux~Mint deriva da Ubuntu.
\par Sito web: \texttt{https://linuxmint.com/}
\end{minipage}

\begin{minipage}{.2\linewidth}
  \includegraphics[width=.9\linewidth]{img/pdf/distro/logo-ubuntu.pdf}
\end{minipage}
\begin{minipage}{.75\linewidth}
\textbf{Ubuntu} è la distribuzione più famosa e diffusa in ambito domestico e di ufficio: essa mette a disposizione numerosi strumenti per semplificare l'amministrazione del sistema anche per coloro che sono alle prime armi, tra cui un fornito software~center che permette di provare e installare numerose applicazioni, anche a pagamento e non libere, con un semplice click. In base all'ambiente grafico di cui viene dotata, assume diversi soprannomi (Kubuntu, Lubuntu, Xubuntu).
Una nuova versione di Ubuntu viene rilasciata ogni sei mesi, ma tali rilasci sono supportati solo per pochi mesi: ogni due anni, in aprile, viene rilasciata una versione detta LTS \emph{Long Term Support} che viene mantenuta per cinque anni.
Ubuntu deriva da Debian.
\par Sito web: \texttt{https://www.ubuntu.com/}
\end{minipage}

\begin{minipage}{.2\linewidth}
  \includegraphics[width=.9\linewidth]{img/pdf/distro/logo-debian.pdf}
\end{minipage}
\begin{minipage}{.75\linewidth}
\textbf{Debian} è una tra le distribuzioni Linux più longeve, e unisce la semplicità d'uso con una estrema stabilità.
Può agevolmente essere adattata alle esigenze più disparate, dall'ambito domestico a quello server, e per questo è molto utilizzata in ambito professionale, per il quale costituisce un buon trampolino di lancio.
Debian effettua un'attenta selezione di tutto il software incluso, prediligendo quello libero, e richiede all'utente di essere mediamente preparato.
I rilasci delle nuove versioni di Debian avvengono molto raramente e solo quando il software di cui è dotata è considerato stabile.
\par Sito web: \texttt{https://www.debian.org/}
\end{minipage}

\begin{minipage}{.2\linewidth}
  \includegraphics[width=.9\linewidth]{img/pdf/distro/logo-redhat.pdf}
\end{minipage}
\begin{minipage}{.75\linewidth}
Anche \textbf{RedHat Enterprise Linux} si colloca tra le distribuzioni più longeve della storia di Linux, ed è rivolta ad un'utenza imprenditoriale.
La società che ne controlla lo sviluppo fornisce supporto a pagamento per la realizzazione di soluzioni dedicate e professionali. Esiste anche una distribuzione derivata mantenuta dalla comunità e che non dispone del supporto a pagamento, chiamata CentOS.
\par Siti web: \texttt{https://www.redhat.com/}, \texttt{https://www.centos.org}
\end{minipage}

\begin{minipage}{.2\linewidth}
  \includegraphics[width=.9\linewidth]{img/pdf/distro/logo-archlinux.pdf}
\end{minipage}
\begin{minipage}{.75\linewidth}
\textbf{Arch Linux} è una distribuzione molto flessibile che può essere utilizzata sia in ambito professionale che in ambito domestico, e può essere adattata a numerosi scopi. Non dispone di rilasci programmati, ma tutto il software viene subito aggiornato all'ultima versione disponibile, secondo un ciclo definito \emph{rolling release}.
Essendo estremamente flessibile, Arch richiede un uso intensivo della riga di comando e fornisce e richiede la conoscenza più approfondita del funzionamento di un sistema Linux.
Come Debian, Arch~Linux è un ottimo trampolino di lancio per il passaggio da distribuzioni domestiche verso distribuzioni professionali.
\par Sito web: \texttt{https://www.archlinux.org/}
\end{minipage}

\begin{minipage}{.2\linewidth}
  \includegraphics[width=.9\linewidth]{img/pdf/distro/logo-slackware.pdf}
\end{minipage}
\begin{minipage}{.75\linewidth}
\textbf{Slackware} è la distribuzione Linux più longeva in assoluto. Fatta eccezione per poco software disponibile già pronto, richiede una conoscenza approfondita di un sistema Linux per essere sfruttata al massimo delle potenzialità.
\par Sito web: \texttt{https://www.slackware.com/}
\end{minipage}

\begin{minipage}{.2\linewidth}
  \includegraphics[width=.9\linewidth]{img/pdf/distro/logo-gentoo.pdf}
\end{minipage}
\begin{minipage}{.75\linewidth}
\textbf{Gentoo} è una distribuzione che dispone di un complesso sistema per l'installazione e l'aggiornamento dei programmi attraverso la loro compilazione da codice sorgente, permettendo di selezionare la configurazione software più adatta alle proprie esigenze e al proprio hardware, così da ottenere le migliori prestazioni.
Come tutte le distribuzioni flessibili, può essere usata in svariati ambiti, domestico, professionale, dedicato, e richiede una conoscenza approfondita del sistema.
\par Sito web: \texttt{https://www.gentoo.org/}
\end{minipage}

Al mondo esistono numerose distribuzioni Linux dedicate agli scopi più disparati: esistono distribuzioni dotate di programmi per la composizione di musica, per il montaggio video, per gestire webradio, per realizzare smart-TV, per la videosorveglianza, per l'archiviazione di file in rete, per l'utilizzo con vecchi computer, per la gestione di modem, per gli smartphone e tante altre.

Tutti i programmi che si trovano su una distribuzione possono essere installati e utilizzati anche su un'altra, tuttavia, il grande vantaggio di utilizzare una distribuzione dedicata permette di poter fare affidamento su di una grande comunità che si dedica costantemente all'ottimizzazione e all'integrazione di queste applicazioni dedicate in un sistema quanto più possibile omogeneo.

In figura \figurename~\ref{fig:linux-distribution-timeline} è visibile una linea del tempo che evidenzia la nascita di numerose distribuzioni Linux nel tempo, e il loro "grado di parentela"; un elenco di tutte le distribuzioni è invece reperibile su \url{http://www.distrowatch.com}

\begin{landscape}
\begin{figure}
	\includegraphics[width=.9\linewidth]{img/pdf/distro/linux-distribution-timeline.pdf}
	\caption{Linux Distribution Timeline: da sinistra verso destra si possono distinguere i tre grandi rami di RedHat, Slackware e Debian. L'immagine è visibile a dimensione reale presso \url{https://commons.wikimedia.org/wiki/File:Linux_Distribution_Timeline.svg}}
	\label{fig:linux-distribution-timeline}
\end{figure}
\end{landscape}
