\chapter{Lavorare con Linux}

Tutte le distribuzioni Linux rivolte ad un'utenza domestica e di ufficio dispongono di un sistema di \emph{pacchettizzazione}, ossia un meccanismo che consente la distribuzione e l'installazione dei programmi di sistema e degli applicativi.

Ogni programma viene inserito in un \emph{pacchetto}, che non è altro che un file compresso che contiene tutti i file necessari all'esecuzione del programma in questione.
Debian, Ubuntu e Linux~Mint utilizzano pacchetti \texttt{deb}, RedHat, CentOS e Fedora pacchetti \texttt{rpm}, Arch~Linux pacchetti \texttt{tar.xz}

Per installare un programma, l'utilizzatore può quindi cercare il pacchetto sul sito del produttore, facendo attenzione a scegliere il giusto formato e la giusta versione per la sua distribuzione.

Tuttavia, le distribuzioni mettono a disposizione anche un pratico strumento detto \emph{package manager}, ossia \emph{gestore di pacchetti}, tramite il quale è possibile scaricare e installare i pacchetti da Internet senza doversi preoccupare di formati e versioni.

\begin{boxino}
Installare sempre le applicazioni usando il gestore della propria distribuzione
\end{boxino}

Il gestore di pacchetti, oltre a semplificare la ricerca, l'installazione e la disinstallazione dei vari programmi, si occupa anche di mantenerli sempre aggiornati all'ultima versione disponibile.

\begin{boxino}
Si ricordi sempre di aggiornare periodicamente il sistema
\end{boxino}

\begin{table}
\centering
\begin{tabular}{| c | c | c | c |}\hline
distribuzione & pacchetti & gestore       & strumento grafico \\ \hline
Arch~Linux    & tar.xz    & pacman        & \\
Debian        & deb       & dpkg + apt    & Synaptic  \\
Fedora        & rpm       & rpm + dnf     & Software  \\
Linux~Mint    & deb       & dpkg + apt    & Gestore Software  \\
SUSE          & rpm       & rpm + zypper  & YaST  \\
Ubuntu        & deb       & dpkg + apt    & Ubuntu Software Center  \\
\hline
\end{tabular}
\caption{Riepilogo su alcuni sistemi di pacchettizzazione}
\label{tab:riepilogo-pacchettizzazione}
\end{table}

\begin{figure}
\centering
\begin{subfigure}{.45\textwidth}
  \includegraphics[width=\linewidth]{img/raster/linuxmint-gestore-software.png}
\end{subfigure}
~
\begin{subfigure}{.45\textwidth}
  \includegraphics[width=\linewidth]{img/raster/linuxmint-gestore-pacchetti.png}
\end{subfigure}

\begin{subfigure}{.45\textwidth}
  \includegraphics[width=\linewidth]{img/raster/fedora-gestore-software.png}
\end{subfigure}
~
\begin{subfigure}{.45\textwidth}
  \includegraphics[width=\linewidth]{img/raster/suse-yast.png}
\end{subfigure}

\caption{In alto, a sinistra, il Gestore~Software di Linux~Mint, tramite il quale si possono cercare, installare e disinstallare le applicazioni; a destra, il Gestore~Pacchetti, tramite il quale si possono cercare, installare e disinstallare i programmi di sistema, e si possono mantenere aggiornati tutti i pacchetti con un semplice click. In basso, a sinistra il Gestore~Software su Fedora; a destra, YaST, il gestore software di SUSE.}
\label{fig:gestori-software}
\end{figure}

Nella \tablename~\ref{tab:riepilogo-pacchettizzazione} è visibile un riepilogo dei vari sistemi di pacchettizzazione sulle distribuzioni più diffuse, mentre in \figurename~\ref{fig:gestori-software} sono visibili i gestori di pacchetti di Linux~Mint, Fedora e SUSE, raggiungibili dal menù principale.

\section{Applicazioni di uso comune}
La maggior parte degli utenti, nell'attività quotidiana, utilizza sempre pochi pochi programmi, che sono fondamentali.
Se ne fornisce un breve elenco qui di seguito.

Tutti i programmi elencati sono software~libero o open~source, e possono essere installati su Windows, Mac~OS~X e Linux. Naturalmente, su Linux è consigliato installarli attraverso il gestore di pacchetti della propria distribuzione.

\begin{minipage}{.2\linewidth}
  \includegraphics[width=.9\linewidth]{img/raster/app/libreoffice.png}
\end{minipage}
\begin{minipage}{.75\linewidth}
\textbf{LibreOffice} è una suite da ufficio, cioè un insieme di programmi per la produttività personale e da ufficio: scrittura di testi, analisi di dati e presentazioni multimediali. I suoi punti di forza sono il vasto supporto a numerosi formati di file e la sua sempre più capillare diffusione all'interno di uffici privati, pubbliche amministrazioni, scuole ed enti di ricerca.
\par Sito web: \texttt{https://it.libreoffice.org/}
\end{minipage}

\begin{minipage}{.2\linewidth}
  \includegraphics[width=.9\linewidth]{img/raster/app/firefox.png}
\end{minipage}
\begin{minipage}{.75\linewidth}
\textbf{Firefox} è un web browser, cioè un programma per navigare sul Web, prodotto da Mozilla. I suoi punti di forza sono la velocità con cui mostra le pagine e l'alto grado di personalizzazione che si può ottenere con i numerosi componenti aggiuntivi installabili.
È disponibile anche per smartphone Android e Apple, e permette di sincronizzare preferiti e cronologia tra i vari dispositivi.
\par Sito web: \texttt{https://www.mozilla.org/it/firefox/products/}
\end{minipage}

\begin{minipage}{.2\linewidth}
  \includegraphics[width=.9\linewidth]{img/raster/app/thunderbird.png}
\end{minipage}
\begin{minipage}{.75\linewidth}
\textbf{Thunderbird} è un client di posta, cioè un programma che consente di gestire, in maniera aggregata, le proprie caselle email e i rispettivi messaggi. Il suo punto di forza è l'alto grado di personalizzazione che si può ottenere con i numerosi compontenti aggiuntivi disponibili. Integra anche un calendario che può essere sincronizzato tra vari dispositivi.
\par Sito web: \texttt{https://www.mozilla.org/it/thunderbird/}
\end{minipage}

\begin{minipage}{.2\linewidth}
  \includegraphics[width=.9\linewidth]{img/raster/app/gimp.png}
\end{minipage}
\begin{minipage}{.75\linewidth}
\textbf{GIMP} è un programma di fotoritocco per la modifica delle fotografie. È arricchito con numerosi plug-in che consentono di compiere le operazioni più disparate: dalla semplice importazione di foto RAW all'assemblaggio di panorami. Inoltre, grazie agli script, è possibile ripetere i ritocchi su più fotografie in maniera automatica o interattiva.
\par Sito web: \texttt{http://www.gimp.org/}
\end{minipage}

\begin{minipage}{.2\linewidth}
  \includegraphics[width=.9\linewidth]{img/raster/app/inkscape.png}
\end{minipage}
\begin{minipage}{.75\linewidth}
\textbf{Inkscape} è un programma di grafica vettoriale, cioè che consente di realizzare disegni scalabili. È adatto per la creazione e la modifica di loghi, volantini e locandine, o per il disegno stilizzato di strisce e fumetti.
\par Sito web: \texttt{https://inkscape.org/it/}
\end{minipage}

\begin{minipage}{.2\linewidth}
  \includegraphics[width=.9\linewidth]{img/raster/app/vlc.png}
\end{minipage}
\begin{minipage}{.75\linewidth}
\textbf{VLC} è un programma per la riproduzione di musica e video. I suoi punti di forza sono il supporto della maggior parte dei formati esistenti, la possibilità di convertire audio e video in altri formati e la trasmissione via rete di flussi video.
\par Sito web: \texttt{http://www.videolan.org/vlc}
\end{minipage}

\begin{minipage}{.2\linewidth}
  \includegraphics[width=.9\linewidth]{img/raster/app/audacity.png}
\end{minipage}
\begin{minipage}{.75\linewidth}
\textbf{Audacity} è un programma di registrazione multitraccia, adatto per la registrazione di più strumenti musicali e la modifica di canzoni. Include anche strumenti per la generazione di suoni e rumori, per la rimozione del rumore di fondo ed il trattamento delle tracce audio. Riesce ad importare ed esportare buona parte dei formati audio.
\par Sito web: \texttt{http://www.audacityteam.org/}
\end{minipage}

\begin{minipage}{.2\linewidth}
  \includegraphics[width=.9\linewidth]{img/raster/app/xmind.png}
\end{minipage}
\begin{minipage}{.75\linewidth}
\textbf{XMind} è un programma per la gestione delle mappe mentali e concettuali, È utile per acquisire idee e organizzarle sotto forma di grafici, condividerle con altre persone e svilupparle in modo collaborativo.
Può essere utilizzato per studio e per lavoro, per la gestione della conoscenza o i verbali di riunione.
\par Sito web: \texttt{http://www.xmind.net/}
\end{minipage}

\begin{minipage}{.2\linewidth}
    \includegraphics[width=.9\linewidth]{img/raster/app/xournal.png}
\end{minipage}
\begin{minipage}{.75\linewidth}
\textbf{Xournal} simula un foglio di carta e può essere usato sia per prendere appunti personali, sia come lavagna, adattandosi ai tablet e alle lavagne elettroniche. Tramite Xournal è possibile scrivere a mano libera, usare schemi e forme geometriche, porre annotazioni su documenti esistenti e produrne di nuovi.
\par Sito web: \texttt{http://xournal.sourceforge.net/}
\end{minipage}

\subsection{Altre risorse utili}
Nel tempo, l'enorme quantità di software libero esistente, ha portato alla nascita di siti web specializzati nella catalogazione e nella ricerca di tali programmi.
Questi portali sono utili sia a chi cerca un programma nuovo, sia a chi conosce già molti programmi proprietari ma cerca delle alternative libere.

Di seguito sono riportati alcuni tra i più noti portali comparativi per orientarsi nel mondo del software libero:

\begin{itemize}
  \item \texttt{https://wiki.ubuntu-it.org/Programmi}
  \item \texttt{https://alternativeto.net/}
  \item \texttt{https://www.linux.it/}
\end{itemize}

