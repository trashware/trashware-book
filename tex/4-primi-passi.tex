\chapter{I primi passi}

\begin{shadequote}
Impareremo a camminare,\\
per mano, insieme, a camminare
\par\emph{Zucchero Fornaciari}
\end{shadequote}

\section{Collegamenti elettrici}
Per funzionare, un computer necessita di essere collegato a tutte le sue periferiche e alla rete elettrica.

Per collegare le varie periferiche si hanno numerosi cavi e spine diverse, ma ogni spina ha una forma e un colore specifico, e si inserisce solo nella corrispondente presa: è perciò impossibile sbagliare i collegamenti, basta fare un po' di attenzione.

Nel caso di un computer portatile, invece, tutte le periferiche principali sono già collegate.

Colleghiamo almeno monitor, tastiera e mouse.

Si noti che, sia l'unità centrale, sia alcune periferiche, necessitano di essere alimentate direttamente dalla tensione di rete, come il monitor e la stampante, che saranno quindi dotate di un proprio cavo di alimentazione da attaccare alla presa della corrente.

Se le abbiamo, possiamo anche collegare la stampante, le casse acustiche, le cuffie, il microfono, la webcam e il cavo di rete.

Nel capitolo riguardante l'hardware questi collegamenti sono stati ampiamente descritti ed illustrati.

\section{Accendere il computer}
\begin{wrapfigure}[12]{R}{3cm}
	\centering
	\includegraphics[width=2.5cm]{img/raster/front-computer-case.png}
	\caption{Il pannello frontale di un case}
	\label{fig:front-computer-case}
\end{wrapfigure}

Per accendere il computer si deve individuare il pulsante di accensione, che si trova nella parte anteriore del case, è grande e contrassegnato da un apposito simbolo, con un cerchio e una linea, come si vede in \figurename~\ref{fig:front-computer-case}.

È sufficiente premerlo per un attimo per far avviare il computer.

\section{Spegnere il computer}
Lo spegnimento del computer è un'operazione che deve essere effettuata via software, cioè impartendo un apposito comando di spegnimento. In seguito al comando, il computer effettuerà tutte le operazioni necessarie al suo corretto spegnimento, dopodiché si spegnerà in autonomia. In un sistema correttamente amministrato, l'operazione richiede, al massimo, poche decine di secondi.

Attenzione: uno spegnimento effettuato in modo scorretto, per esempio staccando la spina della corrente, potrebbe danneggiare il software installato sul computer e provocare la perdita di dati. Raramente, anche l'hardware può venire danneggiato da un simile spegnimento anomalo.

Quando il software è danneggiato o poco manutenuto, può accadere che il computer non si spenga: in tal caso, l'unico modo di spegnerlo consiste nel togliere completamente l'alimentazione, con tutti i rischi che ne conseguono.

\begin{boxino}
Lo spegnimento del computer deve essere sempre effettuato seguendo la corretta procedura via software
\end{boxino}

\begin{figure}
    \centering
    \begin{subfigure}{0.45\textwidth}
        \includegraphics[width=.7\textwidth]{img/raster/poweroff-xp.png}
    \end{subfigure}
    \begin{subfigure}{0.45\textwidth}
        \includegraphics[width=.7\textwidth]{img/raster/poweroff-kde.png}
    \end{subfigure}
    \caption{Schermate di spegnimento di sistemi operativi var\^{i}}
    \label{fig:shutdown-prompts}
\end{figure}

Entriamo adesso nel dettaglio e soffermiamoci su alcune fasi importanti.

\section{Avvio}
\subsection{Power-On Self Test}
La fase immediatamente successiva al comando di accensione si chiama POST (Power-On Self Test): in questa fase, la scheda madre effettua un test delle sue funzionalità e delle periferiche connesse prima di avviare il software.

Sui computer, in seguito al POST, vengono mostrate sul monitor le caratteristiche tecniche della macchina, spesso accompagnate da un breve \emph{beep}, specialmente sui computer più vecchi.
Se il POST rileva dei guasti, vengono emessi più \emph{beep} e il computer non può essere avviato.

\subsection{Boot}
Subito dopo l'operazione di POST avviene il \emph{boot}, ossia il caricamento del nucleo del sistema operativo dalla memoria di massa verso la memoria principale.

Se sul computer sono stati installati più sistemi operativi, durante questa fase è possibile scegliere il sistema da far avviare, attraverso un selettore chiamato \emph{boot manager}. Quasi sempre, se non si sceglie un sistema specifico, dopo un po' di tempo viene avviato quello predefinito.

Nello specifico, in pochi secondi il boot~manager trasferisce solo il nucleo principale del sistema operativo in memoria principale (RAM), dopodiché il controllo viene trasferito a quest'ultimo.

Nota: con il termine \emph{reboot} si indica la sequenza di spegnimento e successivo riavvio automatico del computer.

\section{Sistema operativo}
Il sistema operativo è un complesso programma composto da una serie di programmi più piccoli che gestiscono il funzionamento di ogni pezzo del computer, e permettono agli esseri umani di comunicare con la macchina. Il sistema operativo è quindi un software modulare ed estremamente importante.

Non appena il nucleo del sistema ha ricevuto il controllo dal boot manager, esso controlla l'hardware disponibile sulla macchina e carica i moduli di cui necessita per farlo funzionare appieno.

I vari moduli del sistema operativo vengono normalmente trasferiti dall'hard disk alla memoria principale, e rimangono in essa fino allo spegnimento del computer.

Il sistema operativo si trova sempre su una memoria di massa secondaria: di norma è installato in maniera permanente nel disco fisso del computer, ma può essere anche presente su un dispositivo rimovibile, per esempio su un dischetto.

\subsection{Driver}
Alcuni moduli del sistema operativo permettono alle periferiche di funzionare correttamente e al massimo della loro efficenza, perciò sono molto importanti e, per questo, hanno un nome apposito: \emph{driver}.

Il sistema operativo, quando viene avviato, spesso è in grado di riconoscere numerosi dispositivi perché dispone già dei driver per poterli utilizzare, per esempio per mostrare informazioni sul monitor o reagire ai movimenti del mouse.
Può tuttavia capitare che il sistema non disponga di questi driver, per esempio perché è troppo vecchio rispetto al dispositivo che si vuole utilizzare, o perché semplicemente non esistono driver per quel sistema e per quel dispositivo.

Quando si deve acquistare una periferica, è sempre bene controllare che il sistema operativo disponga già dei driver, oppure che questi vengano forniti a corredo della periferica (per esempio, su un dischetto).
La disponibilità dei driver dipende dal tipo della periferica, dal tipo di sistema operativo e dalla sua versione.

\begin{boxino}
Il sistema operativo gestisce il funzionamento complessivo del computer e delle sue periferiche. Per ogni periferica, il sistema operativo deve essere dotato dell'apposito driver.
\end{boxino}

\section{Programmi applicativi}
Come si è visto nel capitolo sul software, i programmi possono essere suddivisi in programmi di base e programmi applicativi.

Il sistema operativo e i suoi moduli fanno parte dei programmi di base, mentre si dicono programmi applicativi, o applicazioni, quelli che vengono lanciati manualmente dall'utente.

Queste applicazioni possono anche essere programmate per partire in automatico alla fine del caricamento del sistema operativo, ma non per questo costituiscono software di base: non sono infatti indispensabili per il funzionamento del computer e la sua interazione con l'essere umano.

Un'applicazione permette di produrre dei dati (per esempio, scrivere appunti, una lettera o un libro), per usufruire dei dati (per esempio, leggere un libro o vedere un film) e per usufruire di servizi (per esempio, acquistare il biglietto del treno e consultare il proprio conto in banca).

I computer odierni possono eseguire più applicazioni contemporaneamente.

Per esempio, è possibile navigare una enciclopedia sul web e prendere appunti, magari mentre si ascolta della musica in sottofondo, avviando tre applicazioni: un browser per navigare, un word processor per scrivere e un lettore multimediale per riprodurre la musica.

\section{Interfaccia grafica}
L'interfaccia grafica, detta anche \emph{GUI} dall'inglese \emph{Graphical User Interface}, permette di impartire ordini al computer attraverso finestre, menù e icone attivate principalmente attraverso l'utilizzo del mouse.

Le finestre suddividono lo schermo del computer in più regioni, ognuna delle quali è dedicata ad una applicazione. Esse possono essere ingrandite, rimpicciolite e spostate per poter organizzare il proprio ambiente di lavoro come si preferisce. Possono anche essere ridotte a icona, cioè nascoste temporaneamente alla vista, operazione utile quando lo schermo è affollato di finestre, così da portare in evidenza le finestre di altre applicazioni con cui si vuole interagire. Le finestre possono anche essere chiuse, il che equivale alla terminazione delle applicazioni in esse mostrate.

I menù raccolgono operazioni per l'uso di una determinata applicazione; essi possono essere aperti per essere consultati, e le operazioni in essi contenute possono essere scelte per essere eseguite. Alcuni menù possono essere ramificati e possono consentire l'accesso ad altri \emph{sottomenù}. Ad esempio, è possibile spegnere il computer usando il menù principale del proprio sistema operativo, oppure salvare (cioè memorizzare) un documento dopo averlo scritto.

Le icone, in maniera analoga ai menù, rappresentano anch'esse delle operazioni, richiamandole attraverso una figura. In genere vengono utilizzate per le operazioni più comuni, lasciando le operazioni meno utilizzate ``chiuse" all'interno dei menù.

L'interfaccia grafica è presente in tutti gli attuali sistemi operativi, anche se, da un punto di vista tecnico, non fa propriamente parte del software di base.

\subsection{Suggerimenti d'uso sulle interfacce grafiche}
Le interfacce grafiche possono differire in base al sistema operativo utilizzato, alla sua versione e a numerosi altri fattori. Quando si utilizza un'interfaccia grafica risulta perciò indispensabile leggere, interpretare e capire le parole e i simboli utilizzati, senza focalizzarsi eccessivamente sulla loro posizione sullo schermo, sulla loro forma o sul loro colore, astraendo dal contesto: con un po' di allenamento si riesce a farlo, e, nonostante un leggero sforzo iniziale, in questo modo l'uso del computer risulterà molto più facile.
Con questa tecnica si elimina anche la sensazione di smarrimento quando si deve cambiare interfaccia grafica, sensazione tipica degli utenti più superficiali.

\begin{boxino}
Per utilizzare l'interfaccia grafica di un computer è necessario imparare ad astrarre i concetti in essa rappresentati
\end{boxino}

Facciamo alcuni esempi.

\begin{figure}
    \centering
    \begin{subfigure}{0.3\textwidth}
        \includegraphics{img/raster/winbut-win10.png}
    \end{subfigure}
    ~
    \begin{subfigure}{0.3\textwidth}
        \includegraphics{img/raster/winbut-kde5.png}
    \end{subfigure}
    ~
    \begin{subfigure}{0.3\textwidth}
        \includegraphics{img/raster/winbut-mac.png}
    \end{subfigure}
    \caption{Vari bottoni per la gestione delle finestre}
    \label{fig:winbut}
\end{figure}

In \figurename~\ref{fig:winbut} sono mostrati tre diversi modi per la visualizzazione dei pulsanti di gestione delle finestre su tre diversi sistemi operativi: Windows~10, Linux~con~KDE5, Mac~OS~X, rispettivamente da sinistra verso destra.
Per chiudere la finestra, su Windows e KDE si deve premere sulla \emph{X}, che ricorda l'azione di terminazione, mentre su Mac~OS~X, siccome i bottoni ricordano un semaforo, per chiudere una finestra si dovrà usare il bottone rosso.

\begin{figure}
  \centering
  \begin{subfigure}{.2\textwidth}
    \includegraphics{img/raster/win-save.png}
  \end{subfigure}
  ~
  \begin{subfigure}{.2\textwidth}
    \includegraphics{img/raster/kde-save.png}
  \end{subfigure}
  \caption{Vari bottoni di \emph{salvataggio}}
  \label{fig:save-buttons}
\end{figure}

Analogamente, in \figurename~\ref{fig:save-buttons} sono mostrate diverse icone che, seppur diverse, attivano tutte la procedura di salvataggio dei dati: questa azione viene evocata tramite l'uso della figura del classico \emph{disco floppy}, sul quale, appunto, si usa(va) salvare i dati.

\begin{figure}
  \centering

  \begin{subfigure}{.45\textwidth}
    \includegraphics[width=\textwidth]{img/raster/de/desktop-win10.png}
    \caption{Explorer su Windows~10}
  \end{subfigure}
  ~
  \begin{subfigure}{.45\textwidth}
    \includegraphics[width=\textwidth]{img/raster/de/desktop-osx.png}
    \caption{Finder su Mac~Os~X Mountain Lion 10.8}
  \end{subfigure}

  \begin{subfigure}{.45\textwidth}
    \includegraphics[width=\textwidth]{img/raster/de/desktop-unity.png}
    \caption{Unity su Ubuntu~15.04}
  \end{subfigure}
  ~
  \begin{subfigure}{.45\textwidth}
    \includegraphics[width=\textwidth]{img/raster/de/desktop-kde5.png}
    \caption{KDE5 Plasma su Arch~Linux}
  \end{subfigure}

  \begin{subfigure}{.45\textwidth}
    \includegraphics[width=\textwidth]{img/raster/de/desktop-xfce4.png}
    \caption{XFCE4 su Ubuntu}
  \end{subfigure}
  ~
  \begin{subfigure}{.45\textwidth}
    \includegraphics[width=\textwidth]{img/raster/de/desktop-mate.png}
    \caption{MATE 1.14 su Linux~Mint~18 "Sarah"}
  \end{subfigure}

  \caption{Alcune famose interfacce grafiche sui principali sistemi operativi}
\end{figure}

\subsection{Interfaccia testuale}
L'interfaccia testuale, detta anche \emph{CLI}, dall'inglese \emph{Command Line Interface}, permette di impartire ordini al computer attraverso comandi testuali.

Nei computer per uso domestico e d'ufficio, le interfacce testuali affiancano le interfacce grafiche, e le sostituiscono del tutto in molti ambiti professionali avanzati.

Contrariamente a quanto si possa credere, le interfacce testuali sono molto più potenti delle interfacce grafiche e sono ancora largamente utilizzate, in quanto permettono agilmente di amministrare sistemi complessi, di programmare operazioni periodiche o ripetitive e di lavorare a distanza.

Le interfacce testuali non saranno argomento di questo manuale.

\section{Filesystem}
\subsubsection{File}
In informatica, un file (dall'inglese \emph{archivio}) rappresenta un contenitore di informazioni in formato digitale. Ogni file ha un nome, che permette di identificarlo all'interno del \emph{filesystem}, cioè l'insieme di meccanismi atti alla manipolazione dei file.

Esempi di file sono i documenti di testo, le canzoni, le fotografie e quant'altro possa essere memorizzato su di un computer.

Il contenuto dei file è conforme ad un particolare formato, cioè al modo in cui le informazioni in esso contenute vengono organizzate.

Per esempio, le immagini possono essere memorizzate in formato JPEG o PNG, le canzoni in MP3 e OGG.

Per ciascun formato di file esistono una o più applicazioni che sono in grado di interpretare o di modificare il contenuto.

Per esempio, il programma VLC è in grado di riprodurre canzoni in MP3, OGG e numerosi altri formati.

Il formato di un file può essere dedotto dalla sua \emph{estensione}, ossia da quel suffisso che, convenzionalmente, viene apposto dopo un punto, alla fine del nome del file.
Per esempio, un file che si chiama \emph{miafoto.jpeg} viene riconosciuto come una immagine in formato JPEG dal nome \emph{miafoto}.
Alcuni sistemi operativi sono in grado di riconoscere un file anche se questo non ha l'estensione, o ce l'ha sbagliata; tuttavia, poiché esistono sistemi operativi che non sono in grado di farlo, non bisogna mai modificare o eliminare l'estensione, per rispetto degli utenti che debbono usarli.
Le interfacce grafiche di alcuni sistemi operativi, per rendere meno probabile la modifica accidentale dell'estensione, la nascondono. Questa pratica ha anche dei risvolti negativi, tra cui la difficoltà di capire a colpo d'occhio il formato di file con cui si ha a che fare.

\subsubsection{Filesystem}
\begin{wrapfigure}[13]{L}{7cm}
  \includegraphics[width=\linewidth]{img/raster/filesystem-tree-1.png}
  \caption{Rappresentazione di un filesystem}
  \label{fig:filesystem-tree-1}
\end{wrapfigure}

Il filesystem è una delle componenti fondamentali di un sistema operativo moderno: esso permette di organizzare, memorizzare, manipolare e accedere ai dati contenuti nei file.

È una struttura di tipo gerarchico, in cui esiste un nodo principale, detto \emph{cartella radice}, che comprende sotto di sé tutte le altre cartelle. Nelle cartelle possono essere memorizzati i file, come se fossero i documenti in uno schedario; nelle cartelle possono essere memorizzate anche altre cartelle, come se fossero schedari più piccoli all'interno di schedari più grandi. Questo permette di organizzare i propri file secondo lo schema che si preferisce.

Il sistema operativo e tutti i programmi applicativi sono quindi organizzati in numerose cartelle e in numerosi file secondo uno schema logico.

Naturalmente, il sistema operativo prevede anche una cartella speciale dedicata agli utilizzatori umani del computer, la cosiddetta ``cartella personale". Essa può essere conosciuta con nomi diversi a seconda del sistema operativo, ad esempio ``cartella del profilo utente" o ``cartella home". Si ricordi quanto detto a proposito dell'astrazione: indipendentemente dal nome, dalla posizione e dalla figura, il significato è il medesimo.

All'interno della propria cartella personale è possibile creare le cartelle che si vogliono, nelle quali è possibile memorizzare i file che si producono, che si scaricano dalla rete, che si scambiano con dischi e pennine, e così via.
