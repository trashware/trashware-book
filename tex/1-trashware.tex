\chapter{Trashware}

\section{Definizione e storia}

\begin{shadequote}
Trashware: riutilizzo proficuo di computer dismessi per finalità sociali, attraverso la loro riqualificazione e l'installazione di software libero
\par\emph{dal documento Trashware How-To}
\end{shadequote}

Il termine ``trashware", derivato dalle parole inglesi \emph{trash}, cioè spazzatura, e \emph{hardware}, è la pratica di recuperare vecchi computer dismessi e di renderli di nuovo funzionanti per scopi di utilità socio-culturale.
Parte integrante del trashware è l'installazione esclusiva di software libero, tramite il sistema operativo GNU/Linux e var\^{i} applicativi open source, per portare avanti lo spirito di libertà dell'iniziativa.
Il materiale informatico così ottenuto viene consegnato a enti o persone bisognose, in particolar modo legandolo ad iniziative che tentano di colmare il divario digitale (digital divide), ossia la differenza di mezzi a disposizione tra chi è informaticamente alfabetizzato e chi non lo è.

\begin{figure}[tb!]
	\centering
	\includegraphics[width=0.7\linewidth]{img/raster/officina-informatica}
	\caption{L'Officina Informatica, sede del GOLEM -- 2006}
	\label{fig:officina-informatica}
\end{figure}


L'idea di definire la parola \emph{trashware} e l'attività che questa identifica è generalmente attribuita ad alcuni appassionati facenti parte del GOLEM, il LUG (Linux User Group) di Empoli (FI), che, fin dall'anno 2000, si sono dedicati all'organizzazione e alla effettiva realizzazione di questa pratica, definendone i principi e soprattutto la filosofia.

A Empoli, il 13 giugno 2001, presso il Palazzo delle Esposizioni, si tenne un dibattito dal titolo «Software Libero, Sapere Libero, La Free Software Philosophy e le sue applicazioni». Si parlava di come e dove era nato il Software Libero, dei suoi principi, della sua filosofia e dei risultati ottenuti. In quella occasione il GOLEM, presentò il «Progetto trashware»; possiamo dunque affermare, accettando anche qualche smentita, che il termine \emph{trashware} è nato in Italia nel giugno del 2001.

L'attività del trashware si colloca in un ``limbo" informatico che si trova tra la presunta obsolescenza del computer e la sua reale inadeguatezza. Tra questi due punti esiste un lasso di tempo, che può essere anche di 5 anni, in cui il computer, adottando qualche accorgimento, è ancora capace di adempiere alle sue funzioni in modo totalmente efficace.

\section{Obiettivi del trashware}
\begin{itemize}
  \item Salvaguardare l'ambiente: buttare un computer in discarica ogni tre o quattro anni per comprarne uno più nuovo, più potente o più bello è un gesto dal notevole peso ambientale. La riduzione del quantitativo di rifiuti difficili da smaltire e altamente inquinanti da conferire in discarica aiuta a salvaguardare l'ambiente. Ogni comune PC contiene piombo, mercurio, arsenico, berillio, titanio, cobalto. L'enorme mole di PC che ogni anno viene smaltita è fonte di un enorme inquinamento ambientale, che con la pratica del trashware si può ridurre, specialmente quando molte macchine sono ancora funzionanti.
  \item Promuovere il consumo critico: il consumo critico è una modalità di scelta di beni e servizi che non valuta un prodotto solamente per il prezzo, la qualità e l'estetica. Il consumatore critico orienta i propri acquisti (o rinuncia ad essi), in base anche a criteri ambientali e sociali, che prendono in considerazione le modalità di produzione del bene, il suo trasporto, le sue modalità di smaltimento, l'etica del soggetto che lo produce e la tutela dei lavoratori che vi entrano in contatto. Lo scopo del consumatore critico è quello di ridurre al minimo le energie necessarie alla fruizione di un prodotto, contribuendo con le proprie scelte ad indirizzare le politiche dei soggetti protagonisti del mercato, senza che siano essi a condizionare lo stile di vita di tutti noi.
  La tecnologia non dovrebbe servire a creare dei bisogni indotti, ma a far sentire noi consumatori partecipi e in grado di creare uno sviluppo tecnico più adeguato alle nostre reali esigenze.
  Dare all'obsolescenza tecnologica il suo corso naturale e non il frenetico ritmo artificialmente imposto dai produttori, che sfornano continuamente prodotti ``usa e getta", sono gli obiettivi del trashware.
  \item Promuovere la diffusione del software libero e open source: sui computer recuperati è assolutamente necessario installare software libero. Il software non-libero ha infatti diversi tipi di incompatibilità con la natura stessa del progetto.
  Innanzitutto, incompatibilità economiche: l'acquisto delle licenze sarebbe troppo oneroso per i destinatari delle macchine recuperate, e ovviamente è improponibile installare software senza pagare le costosissime licenze d'uso, perché è un'attività illegale.
  Inoltre, vi sono incompatibilità tecnologiche: è spesso difficile reperire versioni di qualunque software non-libero in grado di funzionare in modo corretto sulle macchine oggetto del recupero e, anche qualora ciò fosse possibile, risulterebbe spesso inutile, poiché i prodotti nuovi e aggiornati spesso non hanno caratteristiche accettabili di retrocompatibilità, ma anzi contribuiscono in modo determinante e artificioso all'obsolescenza programmata. Il software libero concede invece l'accesso al proprio codice sorgente e quindi, oltre alla specifica libertà di distribuzione e utilizzo, e al costo generalmente ridotto e spesso nullo, fornisce la fondamentale possibilità di imparare come funzionano gli strumenti che si utilizzano, e di poterli fortemente personalizzare e adattare anche a macchine meno recenti.
  \item Ridurre il divario digitale (digital divide): l'ignoranza e la discriminazione digitale impediscono a chi non ha la possibilità di dotarsi di un computer, oppure di collegarsi a Internet, di poter utilizzare le nuove tecnologie informatiche e di comunicazione. Si tratta prevalentemente di gente povera, giovani studenti, pensionati, associazioni di volontariato, ma non solo: ancora oggi zone rurali, periferiche o montane, scarsamente popolate, non sono coperte dalla banda larga, in quanto rienute non ``interessanti" sotto il profilo economico dai vari fornitori del servizio.
  Internet è oggi un mezzo di lavoro e di business: non essere connessi alla rete, o non avere le competenze di base per farlo con disinvoltura, significa quindi essere relegati ai margini della società.
\end{itemize}

\section{Il futuro del trashware}
  Purtroppo trasmettere queste idee nel tessuto culturale del Paese non è facile.
  Oggi, molte persone sono disposte a donare i loro vecchi computer ancora funzionanti, ma pochi sono pronti a riceverli. Poche sono le realtà che percepiscono un computer ricondizionato come un valore aggiunto, e molti sono convinti, soprattutto riguardo a Linux e al software libero, che i programmi che non sono di diffusione universale siano necessariamente di serie B.
  La diffusione dei moderni e super tecnologici dispositivi usa e getta, non ci rende ottimisti sul futuro del trashware. I recenti successi commerciali dei tablet PC e degli smartphone ci inducono ad una riflessione sulle reali o presunte esigenze degli utenti, e sulla futilità della corsa al ``nuovo", inteso come status symbol. Inoltre, mentre un PC si smonta con un cacciavite, questi dispositivi, che stanno sostituendo i tradizionali computer, non prevedono un facile accesso ai componenti, rendendo difficoltoso e oneroso ogni intervento.
  L'obiettivo del trashware di prolungare la vita dei personal computers si scontra con la realtà del mercato. Detto ciò, rimane comunque utile metterlo in pratica: in questo manuale, impareremo a conoscere il personal computer e tutti i suoi componenti, come smontarlo, rimontarlo e soprattutto valorizzarlo e usarlo consapevoli di quello che facciamo.
